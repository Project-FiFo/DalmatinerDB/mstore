%%%-------------------------------------------------------------------
%%% @author Heinz N. Gies <heinz@Schrodinger>
%%% @copyright (C) 2018, Heinz N. Gies
%%% @doc
%%%
%%% @end
%%% Created : 22 Feb 2018 by Heinz N. Gies <heinz@Schrodinger>
%%%-------------------------------------------------------------------
-module(bench_SUITE).

%% Note: This directive should only be used in test suites.
-compile(export_all).

-include_lib("common_test/include/ct.hrl").

%%--------------------------------------------------------------------
%% COMMON TEST CALLBACK FUNCTIONS
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% @doc
%%  Returns list of tuples to set default properties
%%  for the suite.
%%
%% Function: suite() -> Info
%%
%% Info = [tuple()]
%%   List of key/value pairs.
%%
%% Note: The suite/0 function is only meant to be used to return
%% default data values, not perform any other operations.
%%
%% @spec suite() -> Info
%% @end
%%--------------------------------------------------------------------
suite() ->
    io:format("bla\n"),
    [{timetrap,{minutes,10}}].

%%--------------------------------------------------------------------
%% @doc
%% Initialization before the whole suite
%%
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%% Reason = term()
%%   The reason for skipping the suite.
%%
%% Note: This function is free to add any key/value pairs to the Config
%% variable, but should NOT alter/remove any existing entries.
%%
%% @spec init_per_suite(Config0) ->
%%               Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% @end
%%--------------------------------------------------------------------
init_per_suite(Config) ->
    Config.

%%--------------------------------------------------------------------
%% @doc
%% Cleanup after the whole suite
%%
%% Config - [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% @spec end_per_suite(Config) -> _
%% @end
%%--------------------------------------------------------------------
end_per_suite(_Config) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Initialization before each test case group.
%%
%% GroupName = atom()
%%   Name of the test case group that is about to run.
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding configuration data for the group.
%% Reason = term()
%%   The reason for skipping all test cases and subgroups in the group.
%%
%% @spec init_per_group(GroupName, Config0) ->
%%               Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% @end
%%--------------------------------------------------------------------
init_per_group(_GroupName, Config) ->
    Config.

%%--------------------------------------------------------------------
%% @doc
%% Cleanup after each test case group.
%%
%% GroupName = atom()
%%   Name of the test case group that is finished.
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding configuration data for the group.
%%
%% @spec end_per_group(GroupName, Config0) ->
%%               term() | {save_config,Config1}
%% @end
%%--------------------------------------------------------------------
end_per_group(_GroupName, _Config) ->
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Initialization before each test case
%%
%% TestCase - atom()
%%   Name of the test case that is about to be run.
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%% Reason = term()
%%   The reason for skipping the test case.
%%
%% Note: This function is free to add any key/value pairs to the Config
%% variable, but should NOT alter/remove any existing entries.
%%
%% @spec init_per_testcase(TestCase, Config0) ->
%%               Config1 | {skip,Reason} | {skip_and_save,Reason,Config1}
%% @end
%%--------------------------------------------------------------------
init_per_testcase(_TestCase, Config) ->
    os:cmd("rm -r bench"),
    os:cmd("rm -r bench.hpts"),
    Size = proplists:get_value(file_size, Config, 20000),
    Count = proplists:get_value(count, Config, Size div 2),
    {ok, Store} = mstore:new("bench", [{file_size, Size}, {flags, []}]),
    L1 = lists:seq(0, Count),
    Bin = mmath_bin:from_list(L1),
    Store1 = mstore:put(Store, <<"data">>, 0, Bin),
    mstore:close(Store1),
    {ok, NewStore} = mstore:open("bench"),
    {ok, StoreHPTS} = mstore:new("bench.hpts", [{file_size, Size}, {flags, [hpts]}]),
    L2 = [{E, E} || E <- L1],
    Bin2 = mmath_hpts:from_list(L2),
    StoreHPTS1 = mstore:put(StoreHPTS, <<"data">>, 0, Bin2),
    mstore:close(StoreHPTS1),
    {ok, NewStoreHPTS} = mstore:open("bench.hpts"),
    [{file_size, Size}, {count, Count}, {store, NewStore}, {hpts, NewStoreHPTS} | Config].


%%--------------------------------------------------------------------
%% @doc
%% Cleanup after each test case
%%
%% TestCase - atom()
%%   Name of the test case that is finished.
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%%
%% @spec end_per_testcase(TestCase, Config0) ->
%%               term() | {save_config,Config1} | {fail,Reason}
%% @end
%%--------------------------------------------------------------------
end_per_testcase(_TestCase, Config) ->
    case proplists:lookup(store, Config) of
        {store, Store} ->
            mstore:close(Store);
        _ ->
            ok
    end,
    os:cmd("rm -r bench"),
    os:cmd("rm -r bench.hpts"),
    ok.

%%--------------------------------------------------------------------
%% @doc
%% Returns a list of test case group definitions.
%%
%% Group = {GroupName,Properties,GroupsAndTestCases}
%% GroupName = atom()
%%   The name of the group.
%% Properties = [parallel | sequence | Shuffle | {RepeatType,N}]
%%   Group properties that may be combined.
%% GroupsAndTestCases = [Group | {group,GroupName} | TestCase]
%% TestCase = atom()
%%   The name of a test case.
%% Shuffle = shuffle | {shuffle,Seed}
%%   To get cases executed in random order.
%% Seed = {integer(),integer(),integer()}
%% RepeatType = repeat | repeat_until_all_ok | repeat_until_all_fail |
%%              repeat_until_any_ok | repeat_until_any_fail
%%   To get execution of cases repeated.
%% N = integer() | forever
%%
%% @spec: groups() -> [Group]
%% @end
%%--------------------------------------------------------------------
groups() ->
    [].

%%--------------------------------------------------------------------
%% @doc
%%  Returns the list of groups and test cases that
%%  are to be executed.
%%
%% GroupsAndTestCases = [{group,GroupName} | TestCase]
%% GroupName = atom()
%%   Name of a test case group.
%% TestCase = atom()
%%   Name of a test case.
%% Reason = term()
%%   The reason for skipping all groups and test cases.
%%
%% @spec all() -> GroupsAndTestCases | {skip,Reason}
%% @end
%%--------------------------------------------------------------------
all() -> 
    [read_time_case, hpts_values_case, read_time_hpts_raw_case, read_time_hpts_case, read_time_hpts_seperate_case].


%%--------------------------------------------------------------------
%% TEST CASES
%%--------------------------------------------------------------------

%%--------------------------------------------------------------------
%% @doc 
%%  Test case info function - returns list of tuples to set
%%  properties for the test case.
%%
%% Info = [tuple()]
%%   List of key/value pairs.
%%
%% Note: This function is only meant to be used to return a list of
%% values, not perform any other operations.
%%
%% @spec TestCase() -> Info 
%% @end
%%--------------------------------------------------------------------
read_time_case() ->
    [].

read_clean_time_case() ->
    [].

%%--------------------------------------------------------------------
%% @doc Test case function. (The name of it must be specified in
%%              the all/0 list or in a test case group for the test case
%%              to be executed).
%%
%% Config0 = Config1 = [tuple()]
%%   A list of key/value pairs, holding the test case configuration.
%% Reason = term()
%%   The reason for skipping the test case.
%% Comment = term()
%%   A comment about the test case that will be printed in the html log.
%%
%% @spec TestCase(Config0) ->
%%           ok | exit() | {skip,Reason} | {comment,Comment} |
%%           {save_config,Config1} | {skip_and_save,Reason,Config1}
%% @end
%%--------------------------------------------------------------------
-define(TOPTS, [{iterations, 10000}, {concurrency, 1}]).
read_time_case(Config) ->
    {store, Store} = proplists:lookup(store, Config),
    {count, Count} = proplists:lookup(count, Config),
    R = timing_hdr:run(fun() ->
                               {ok, _Bin} = mstore:get(Store, <<"data">>, 0, Count),
                               ok
                       end, [{name, "Read once"} | ?TOPTS]),
    result("Read", R),
    ok.

read_time_hpts_raw_case(Config) ->
    {hpts, Store} = proplists:lookup(hpts, Config),
    {count, Count} = proplists:lookup(count, Config),
    R = timing_hdr:run(fun() ->
                               {ok, _Bin} = mstore:get(Store, <<"data">>, 0, Count, [with_timestamp]),
                               ok
                       end, [{name, "Read HPTS w/ timestamp"} | ?TOPTS]),
    result("HPTS", R),
    ok.

read_time_hpts_seperate_case(Config) ->
    {hpts, Store} = proplists:lookup(hpts, Config),
    {count, Count} = proplists:lookup(count, Config),
    R = timing_hdr:run(fun() ->
                               {ok, Bin} = mstore:get(Store, <<"data">>, 0, Count, [with_timestamp]),
                               mmath_hpts:values(Bin),
                               ok
                       end, [{name, "Read HPTS w/ timestamp"} | ?TOPTS]),
    result("HPTS", R),
    ok.

hpts_values_case(Config) ->
    {hpts, Store} = proplists:lookup(hpts, Config),
    {count, Count} = proplists:lookup(count, Config),
    {ok, Bin} = mstore:get(Store, <<"data">>, 0, Count, [with_timestamp]),
    R = timing_hdr:run(fun() ->
                               mmath_hpts:values(Bin),
                               ok
                       end, [{name, "HPTS values"} | ?TOPTS]),
    result("HPTS", R),
    ok.

read_time_hpts_case(Config) ->
    {hpts, Store} = proplists:lookup(hpts, Config),
    {count, Count} = proplists:lookup(count, Config),
    R = timing_hdr:run(fun() ->
                               {ok, _Bin} = mstore:get(Store, <<"data">>, 0, Count),
                               ok
                       end, [{name, "Read HPTS w/o timestamp"} | ?TOPTS]),
    result("HPTS", R),
    ok.


result(_Case, Time) ->
    io:format(user, "~p~n", [Time]).
